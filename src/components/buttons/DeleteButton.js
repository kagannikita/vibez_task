import React, { useState } from "react";

// Chakra imports
import { Button, Flex, Text } from "@chakra-ui/react";

export function DeleteButton({ action, isPassive }) {
  const [isOpen, setIsOpen] = useState(false);

  const handleDelete = () => {
    setIsOpen(false);
    action();
  };

  return (
    <Flex>
      {isOpen && (
        <Flex
          direction={"column"}
          w="100%"
          h="100%"
          position={"fixed"}
          right="0px"
          top="0px"
          bgColor="white"
          zIndex={99999}
          justifyContent={"center"}
          textAlign={"center"}
          alignItems={"center"}
        >
          <Text mb={"40px"}>Are you sure you want to Delete?</Text>
          <Button
            onClick={() => setIsOpen(false)}
            size="lg"
            variant="brand"
            fontSize="md"
            fontWeight="500"
            borderRadius="70px"
            px="24px"
            py="5px"
            mb="20px"
            w="300px"
          >
            {"GO BACK"}
          </Button>
          <Button
            onClick={() => handleDelete()}
            size="lg"
            variant="outline"
            fontSize="md"
            fontWeight="500"
            borderRadius="70px"
            px="24px"
            py="5px"
            w="300px"
          >
            {"DELETE"}
          </Button>
        </Flex>
      )}
      <Button
        onClick={() => setIsOpen(true)}
        variant={isPassive ? "outline" : "brand"}
        minW={"100px"}
        fontSize="sm"
        fontWeight="500"
        borderRadius="70px"
        px="24px"
        py="5px"
        mx="5px"
      >
        {"Delete"}
      </Button>
    </Flex>
  );
}
